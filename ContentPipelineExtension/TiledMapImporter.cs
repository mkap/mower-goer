﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content.Pipeline;

using TInput = System.String;

namespace ContentPipelineExtension
{
    [ContentImporter(".json", DisplayName = "Tiled Map Importer", DefaultProcessor = "TiledMapProcessor")]
    public class TiledMapImporter : ContentImporter<TInput>
    {
        public override TInput Import(string filename, ContentImporterContext context)
        {
            context.Logger.LogMessage($"Importing Tiled map json file: {filename}.");

            using (var streamReader = new StreamReader(filename))
            {
                return streamReader.ReadToEnd();
            }
        }
    }
}
