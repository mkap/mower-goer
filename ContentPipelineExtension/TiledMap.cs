﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContentPipelineExtension
{
    public class TiledMap
    {
        public int Height { get; set; }
        public int Width { get; set; }
        public int TileWidth { get; set; }
        public int TileHeight { get; set; }
        public List<Layer> Layers { get; set; } = new List<Layer>();
        public List<Tileset> Tilesets { get; set; } = new List<Tileset>();
    }

    public class Layer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public List<int> Data { get; set; } = new List<int>();
        public List<Object> Objects { get; set; } = new List<Object>();
    }

    public class Object
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public List<Property> Properties { get; set; } = new List<Property>();
    }

    public class Property
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }
    }

    public class Tileset
    {
        public int Firstgid { get; set; }
        public string Source { get; set; }
        public string Name { get; set; }
    }
}
