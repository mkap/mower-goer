﻿using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TWrite = ContentPipelineExtension.TiledMap;

namespace ContentPipelineExtension
{
    [ContentTypeWriter]
    public class TiledMapWriter : ContentTypeWriter<TWrite>
    {
        protected override void Write(ContentWriter output, TWrite value)
        {
            // write the map properties
            output.Write(value.Width);
            output.Write(value.Height);
            output.Write(value.TileWidth);
            output.Write(value.TileHeight);
            output.Write(value.Layers.Count);
            output.Write(value.Tilesets.Count);

            // write the tile layers
            foreach (var layer in value.Layers)
            {
                if (layer.Type.Equals("tilelayer"))
                {
                    output.Write(layer.Id);
                    output.Write(layer.Name);
                    output.Write(layer.Type);

                    foreach (var tileIndex in layer.Data)
                    {
                        output.Write(tileIndex);
                    }
                }
            }

            // write the object layers
            foreach (var layer in value.Layers)
            {
                if (layer.Type.Equals("objectgroup"))
                {
                    output.Write(layer.Id);
                    output.Write(layer.Name);
                    output.Write(layer.Type);
                    output.Write(layer.Objects.Count);

                    foreach (var obj in layer.Objects)
                    {
                        output.Write(obj.Name);
                        output.Write(obj.Type);
                        output.Write(obj.X);
                        output.Write(obj.Y);
                        output.Write(obj.Width);
                        output.Write(obj.Height);
                        output.Write(obj.Properties.Count);

                        foreach (var property in obj.Properties)
                        {
                            output.Write(property.Name);
                            output.Write(property.Type);
                            output.Write(property.Value);
                        }
                    }
                }
            }

            // write the tilesets
            foreach (var tileset in value.Tilesets)
            {
                output.Write(tileset.Firstgid);
                output.Write(tileset.Source);
                output.Write(tileset.Name);
            }
        }

        public override string GetRuntimeReader(TargetPlatform targetPlatform)
        {
            return "ContentPipelineExtension.TiledMapReader, ContentPipelineExtension";
        }
    }
}
