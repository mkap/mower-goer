﻿using Microsoft.Xna.Framework.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TRead = ContentPipelineExtension.TiledMap;

namespace ContentPipelineExtension
{
    public class TiledMapReader : ContentTypeReader<TRead>
    {
        protected override TRead Read(ContentReader input, TRead existingInstance)
        {
            var width = input.ReadInt32();
            var height = input.ReadInt32();
            var tileWidth = input.ReadInt32();
            var tileHeight = input.ReadInt32();
            var layerCount = input.ReadInt32();
            var tilesetCount = input.ReadInt32();

            var map = new TiledMap()
            {
                Width = width,
                Height = height,
                TileWidth = tileWidth,
                TileHeight = tileHeight
            };

            for (int i = 0; i < layerCount; i++)
            {
                var layer = new Layer();
                layer.Id = input.ReadInt32();
                layer.Name = input.ReadString();
                layer.Type = input.ReadString();

                if (layer.Type.Equals("tilelayer"))
                {
                    for (int j = 0; j < map.Width * map.Height; j++)
                    {
                        layer.Data.Add(input.ReadInt32());
                    }
                }
                else if (layer.Type.Equals("objectgroup"))
                {
                    var objectCount = input.ReadInt32();
                    for (int j = 0; j < objectCount; j++)
                    {
                        var obj = new Object();
                        obj.Name = input.ReadString();
                        obj.Type = input.ReadString();
                        obj.X = input.ReadInt32();
                        obj.Y = input.ReadInt32();
                        obj.Width = input.ReadInt32();
                        obj.Height = input.ReadInt32();

                        var propertyCount = input.ReadInt32();
                        for (int k = 0; k < propertyCount; k++)
                        {
                            var property = new Property();
                            property.Name = input.ReadString();
                            property.Type = input.ReadString();
                            property.Value = input.ReadString();

                            obj.Properties.Add(property);
                        }

                        layer.Objects.Add(obj);
                    }
                }
                
                map.Layers.Add(layer);
            }

            for (int i = 0; i < tilesetCount; i++)
            {
                var tileset = new Tileset();
                tileset.Firstgid = input.ReadInt32();
                tileset.Source = input.ReadString();
                tileset.Name = input.ReadString();

                map.Tilesets.Add(tileset);
            }

            return map;
        }
    }
}
