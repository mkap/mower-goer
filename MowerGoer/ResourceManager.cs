﻿using ContentPipelineExtension;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MowerGoer
{
    public enum TextureName
    {
        Player, PlayerHit, GroundTileset, ObstacleTileset, RuinedObstacleTileset, PixelParticle, TitleScreen
    }

    public enum SoundEffectName
    {
        EngineIdle, Countdown, Go, Stop, Start, Obstacle
    }

    public enum FontName
    {
        DigitalDiscoFont, DigitalDiscoBigFont
    }

    public enum SongName
    {
        SubduedTheme, HappyLevel
    }
    public static class ResourceManager
    {
        private static MainGame _mainGame;

        private static Dictionary<TextureName, Texture2D> _textures = new Dictionary<TextureName, Texture2D>();
        private static Dictionary<SoundEffectName, SoundEffect> _soundEffects = new Dictionary<SoundEffectName, SoundEffect>();
        private static Dictionary<FontName, SpriteFont> _fonts = new Dictionary<FontName, SpriteFont>();
        private static Dictionary<SongName, Song> _songs = new Dictionary<SongName, Song>();

        public static void Initialize(MainGame mainGame)
        {
            _mainGame = mainGame;
        }

        public static void LoadAllTextures()
        {
            _textures.Add(TextureName.Player, _mainGame.Content.Load<Texture2D>("Player"));
            _textures.Add(TextureName.PlayerHit, _mainGame.Content.Load<Texture2D>("PlayerHit"));
            _textures.Add(TextureName.GroundTileset, _mainGame.Content.Load<Texture2D>("GroundTileset"));
            _textures.Add(TextureName.ObstacleTileset, _mainGame.Content.Load<Texture2D>("ObstacleTileset"));
            _textures.Add(TextureName.RuinedObstacleTileset, _mainGame.Content.Load<Texture2D>("RuinedObstacleTileset"));
            _textures.Add(TextureName.PixelParticle, _mainGame.Content.Load<Texture2D>("PixelParticle"));
            _textures.Add(TextureName.TitleScreen, _mainGame.Content.Load<Texture2D>("TitleScreen"));
        }

        public static void LoadAllSoundEffects()
        {
            _soundEffects.Add(SoundEffectName.EngineIdle, _mainGame.Content.Load<SoundEffect>("EngineIdle"));
            _soundEffects.Add(SoundEffectName.Countdown, _mainGame.Content.Load<SoundEffect>("Countdown"));
            _soundEffects.Add(SoundEffectName.Go, _mainGame.Content.Load<SoundEffect>("Go"));
            _soundEffects.Add(SoundEffectName.Stop, _mainGame.Content.Load<SoundEffect>("Stop"));
            _soundEffects.Add(SoundEffectName.Start, _mainGame.Content.Load<SoundEffect>("Start"));
            _soundEffects.Add(SoundEffectName.Obstacle, _mainGame.Content.Load<SoundEffect>("Obstacle"));
        }

        public static void LoadAllFonts()
        {
            _fonts.Add(FontName.DigitalDiscoFont, _mainGame.Content.Load<SpriteFont>("DigitalDiscoFont"));
            _fonts.Add(FontName.DigitalDiscoBigFont, _mainGame.Content.Load<SpriteFont>("DigitalDiscoBigFont"));
        }

        public static void LoadAllSongs()
        {
            _songs.Add(SongName.SubduedTheme, _mainGame.Content.Load<Song>("SubduedTheme"));
            _songs.Add(SongName.HappyLevel, _mainGame.Content.Load<Song>("HappyLevel"));
        }

        public static Texture2D GetTexture(TextureName name)
        {
            return _textures[name];
        }

        public static SoundEffect GetSoundEffect(SoundEffectName name)
        {
            return _soundEffects[name];
        }

        public static SpriteFont GetFont(FontName name)
        {
            return _fonts[name];
        }

        public static Song GetSong(SongName name)
        {
            return _songs[name];
        }

        public static TiledMap LoadMap(string name)
        {
            return _mainGame.Content.Load<TiledMap>(name);
        }

        public static Texture2D LoadTexture(string name)
        {
            return _mainGame.Content.Load<Texture2D>(name);
        }
    }
}
