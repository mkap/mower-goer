﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MowerGoer
{
    public class Player
    {
        public Texture2D Texture { get; set; }
        public Vector2 Position { get; set; }
        public Vector2 DrawOffset { get; set; }
        public Vector2 Velocity { get; set; }
        public float Heading { get; set; }
        public float Speed { get; set; }
        public float SteeringAngle { get; set; }
        public Circle CuttingArea { get; set; }
        public bool IsTurbo { get; set; }

        private float _engineIdleCounterMs;
        private ParticleEffect _backParticleEffect = new ParticleEffect();
        private float _obstacleHitTimeElapsedMs;
        private bool _hitObstacle;
        private ObstacleType _obstacleType;

        public void Initialize()
        {
            Position = Constants.StartPosition;
            Heading = (float)Math.Atan2(-1, 0);
            CuttingArea = new Circle((int)Math.Round(Position.X, 0), (int)Math.Round(Position.Y, 0), Constants.CuttingRadius);
            _engineIdleCounterMs = 0;

            _backParticleEffect.Initialize(Position, ResourceManager.GetTexture(TextureName.PixelParticle));
            _obstacleHitTimeElapsedMs = 0;
            _hitObstacle = false;
            Texture = ResourceManager.GetTexture(TextureName.Player);
        }

        public void Update(GameTime gameTime)
        {
            var frontWheel = Position + Constants.WheelBase / 2 * new Vector2((float)Math.Cos(Heading), (float)Math.Sin(Heading));
            var backWheel = Position - Constants.WheelBase / 2 * new Vector2((float)Math.Cos(Heading), (float)Math.Sin(Heading));
            backWheel += Speed * gameTime.ElapsedGameTime.Milliseconds * new Vector2((float)Math.Cos(Heading), (float)Math.Sin(Heading));
            frontWheel += Speed * gameTime.ElapsedGameTime.Milliseconds * new Vector2((float)Math.Cos(Heading + SteeringAngle), (float)Math.Sin(Heading + SteeringAngle));

            Position = (frontWheel + backWheel) / 2;
            AdjustForBoundaries();

            Heading = (float)Math.Atan2(frontWheel.Y - backWheel.Y, frontWheel.X - backWheel.X);

            CuttingArea = new Circle((int)Math.Round(Position.X, 0), (int)Math.Round(Position.Y, 0), Constants.CuttingRadius);

            DampenSpeed();
            DampenSteeringAngle();

            _engineIdleCounterMs += gameTime.ElapsedGameTime.Milliseconds;
            if (_engineIdleCounterMs >= (IsTurbo ? Constants.TurboEngineIdleMs : Constants.NormalEngineIdleMs))
            {
                DrawOffset = DrawOffset.Y == 0 ? new Vector2(0, 1) : Vector2.Zero;
                _engineIdleCounterMs = 0;
                ResourceManager.GetSoundEffect(SoundEffectName.EngineIdle).Play(0.005f, 0, 0);
            }

            if (_hitObstacle)
            {
                _obstacleHitTimeElapsedMs += gameTime.ElapsedGameTime.Milliseconds;
                if (_obstacleHitTimeElapsedMs >= Constants.ObstacleHitTimeMs)
                {
                    Texture = ResourceManager.GetTexture(TextureName.Player);
                    _obstacleHitTimeElapsedMs = 0;
                    _hitObstacle = false;
                }
                CreateParticles(32, _obstacleType);
            }


            _backParticleEffect.Update(gameTime);
            _backParticleEffect.Position = backWheel;
        }

        private void AdjustForBoundaries()
        {
            var x = Position.X;
            var y = Position.Y;

            if (x < 0)
            {
                x = 320;
            }
            if (x > 320)
            {
                x = 0;
            }
            if (y < 0)
            {
                y = 180;
            }
            if (y > 180)
            {
                y = 0;
            }

            Position = new Vector2(x, y);
        }

        private void DampenSpeed()
        { 
            Speed -= 0.005f;
            if (Speed < 0)
            {
                Speed = 0;
            }
        }

        private void DampenSteeringAngle()
        {
            if (SteeringAngle > 0)
            {
                SteeringAngle -= 0.01f;
                if (SteeringAngle < 0)
                {
                    SteeringAngle = 0;
                }
            }
            else if (SteeringAngle < 0)
            {
                SteeringAngle += 0.01f;
                if (SteeringAngle > 0)
                {
                    SteeringAngle = 0;
                }
            }
        }

        public void UpdateSteeringAngle(float amount)
        {
            SteeringAngle += amount;
            if (SteeringAngle > Constants.MaximumSteeringAngle)
            {
                SteeringAngle = Constants.MaximumSteeringAngle;
            }
            else if (SteeringAngle < Constants.MinimumSteeringAngle)
            {
                SteeringAngle = Constants.MinimumSteeringAngle;
            }
        }

        public void UpdateSpeed(float amount)
        {
            var multiplier = IsTurbo ? Constants.TurboPlayerSpeed : Constants.NormalPlayerSpeed;
            var maximum = IsTurbo ? Constants.MaximumTurboPlayerSpeed : Constants.MaximumNormalPlayerSpeed;
            Speed += amount * multiplier;
            if (Speed > maximum)
            {
                Speed = maximum;
            }
        }

        public void CreateParticles(int amount, ObstacleType type)
        {
            for (int i = 0; i < amount; i++)
            {
                var color = GetParticleColor(type);
                _backParticleEffect.CreateParticle(color);
            }
        }

        private Color GetParticleColor(ObstacleType type)
        {
            switch (type)
            {
                default:
                case ObstacleType.None:
                    return Constants.GrassColors[Constants.Random.Next(2)];

                case ObstacleType.Rocks:
                    return Constants.RockColors[Constants.Random.Next(2)];

                case ObstacleType.Flowers:
                    return Constants.FlowerColors[Constants.Random.Next(2)];

                case ObstacleType.Sticks:
                    return Constants.StickColors[Constants.Random.Next(2)];

                case ObstacleType.Bush:
                    return Constants.BushColors[Constants.Random.Next(2)];
            }
        }

        public List<Particle> GetParticles()
        {
            return _backParticleEffect.Particles;
        }

        public void HitObstacle(ObstacleType obstacleType)
        {
            _hitObstacle = true;
            _obstacleType = obstacleType;
            _obstacleHitTimeElapsedMs = 0;
            Texture = ResourceManager.GetTexture(TextureName.PlayerHit);
        }
    }
}