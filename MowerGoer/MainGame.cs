﻿using ContentPipelineExtension;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using MowerGoer.States;
using System;
using System.Collections.Generic;

namespace MowerGoer
{ 
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class MainGame : Game
    {
        private GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;
        private Dictionary<StateName, State> _states = new Dictionary<StateName, State>();
        private State _currentState;

        public MainGame()
        {
            _graphics = new GraphicsDeviceManager(this);
            _graphics.PreferredBackBufferWidth = 1280;
            _graphics.PreferredBackBufferHeight = 720;
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            ResourceManager.Initialize(this);

            _states.Add(StateName.Loading, new LoadingState(this));
            _states.Add(StateName.StartScreen, new StartScreenState(this));
            _states.Add(StateName.CountDown, new CountdownState(this));
            _states.Add(StateName.GamePlay, new GamePlayState(this));
            _states.Add(StateName.Results, new ResultsState(this));
            _states.Add(StateName.MapChange, new MapChangeState(this));
            _states.Add(StateName.End, new EndState(this));
            _states.Add(StateName.Stop, new StopState(this));

            MediaPlayer.IsRepeating = true;
            MediaPlayer.Volume = 0.1f;

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            _spriteBatch = new SpriteBatch(GraphicsDevice);

            ResourceManager.LoadAllTextures();
            ResourceManager.LoadAllSoundEffects();
            ResourceManager.LoadAllFonts();
            ResourceManager.LoadAllSongs();

            ChangeState(StateName.Loading);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            _currentState.Update(gameTime);

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            _currentState.Draw(_spriteBatch);
            base.Draw(gameTime);
        }

        public void ChangeState(StateName name)
        {
            _currentState = _states[name];
            _currentState.OnEntry();
        }
    }
}
