﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MowerGoer
{
    public enum TileType
    {
        Grass, Other
    }


    public class Tile
    {
        public Vector2 Position { get; private set; }
        public Texture2D Texture { get; private set; }
        public Rectangle SourceRectangle { get; private set; }
        public TileType TileType { get; private set; }

        public Tile(Vector2 position, Texture2D texture, Rectangle sourceRectangle, TileType tileType)
        {
            Position = position;
            Texture = texture;
            SourceRectangle = sourceRectangle;
            TileType = tileType;
        }
    }
}
