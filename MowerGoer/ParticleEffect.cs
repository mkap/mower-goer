﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MowerGoer
{
    public class ParticleEffect
    {
        private Random _random = new Random();
        private Texture2D _texture;

        public Vector2 Position { get; set; }
        public List<Particle> Particles { get; set; } = new List<Particle>();

        public void Initialize(Vector2 position, Texture2D texture)
        {
            Position = position;
            _texture = texture;
            Particles.Clear();
        }

        public void Update(GameTime gameTime)
        {
            for (int i = Particles.Count - 1; i >= 0; i--)
            {
                Particles[i].Update(gameTime);

                if (!Particles[i].IsAlive)
                {
                    Particles.RemoveAt(i);
                }
            }
        }

        public void CreateParticle(Color color)
        {
            var particle = new Particle();
            var velocity = new Vector2(1.0f * (float)_random.NextDouble() * 2 - 1, 1.0f * (float)_random.NextDouble() * 2 - 1) * 2;
            particle.Initialize(
                _texture,
                Position,
                velocity,
                0f,
                0.1f * (float)(_random.NextDouble() * 2 - 1),
                color,
                1f,
                180f
            );

            Particles.Add(particle);
        }
    }
}
