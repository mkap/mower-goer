﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MowerGoer
{
    public enum ObstacleType
    {
        None, Rocks, Flowers, Sticks, Bush
    }
    public class Obstacle
    {
        public Point[] Pixels { get; set; }
        public int NegativePointValue { get; set; }
        public bool IsDestroyed { get; set; }

        public ObstacleType ObstacleType { get; set; }

        public Obstacle(Point[] pixels, int negativePointValue, ObstacleType obstacleType)
        {
            Pixels = pixels;
            NegativePointValue = negativePointValue;
            ObstacleType = obstacleType;
        }
    }
}