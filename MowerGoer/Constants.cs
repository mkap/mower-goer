﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MowerGoer
{
    public static class Constants
    {
        public static int StartingLevelIndex { get; } = 0;
        public static int LevelCount { get; } = 20;
        public static Random Random { get; } = new Random();

        public static string HighScoreFilename { get; } = "highscore.dat";

        public static int DefaultHighScore { get; } = 200000;

        public readonly static string[] LevelNames = new string[]
        {
            "Let's Roll",
            "Sticks and Stones",
            "At a Crossroads",
            "Lumberyard",
            "Mulberry Lane",
            "Flower Power",
            "Jail Break",
            "The Truth Is Out There",
            "Jammin",
            "Iron Butterfly",
            "Twin Peaks",
            "Down the Drain",
            "Pick Up Sticks",
            "Boulderdash",
            "X Marks the Spot",
            "Wine Country",
            "Zigzagging",
            "Highway Robbery",
            "Iron Fortress",
            "Grand Prix"
        };

        public readonly static int[] ObstacleValues = new int[]
        {
            0,
            -512,
            -2048,
            -256,
            -1024
        };

        // general
        public static Vector2 StartPosition { get; } = new Vector2(160, 150);
        public static int TileSize { get; } = 16;
        public static int MapRows { get; } = 12;
        public static int MapColumns { get; } = 20;


        // timing
        public static float MapChangeTimeMs = 1000f;

        public static float StopTimeMs = 3000f;

        public static float ResultsInputTimeMs = 3000f;
        public static float EndScreenInputTimeMs { get; } = 2000f;
        public static float EndScreenFadeoutTimeMs { get; } = 3000f;
        public static float ObstacleHitTimeMs { get; } = 250f;
        public static float NormalEngineIdleMs { get; } = 80f;
        public static float TurboEngineIdleMs { get; } = 50f;
        public static float StartScreenFadeoutTimeMs { get; } = 2000f;
        public static float StartScreenInputTimeMs { get; } = 3000f;
        public static int RoundTimeSeconds { get; } = 10;

        // player physics
        public static float NormalPlayerSpeed { get; } = 0.009f;
        public static float TurboPlayerSpeed { get; } = 0.026f;
        public static float MaximumNormalPlayerSpeed { get; } = 0.15f;
        public static float MaximumTurboPlayerSpeed { get; } = 0.27f;
        public static int CuttingRadius { get; } = 9;
        public static float WheelBase { get; } = 12f;
        public static float MaximumSteeringAngle { get; } = MathHelper.PiOver2 / 4;
        public static float MinimumSteeringAngle { get; } = -MathHelper.PiOver2 / 4;

        // particle colors
        public readonly static Color[] GrassColors = new Color[]
{
            new Color(117, 179, 37),
            new Color(166, 226, 68),
            new Color(100, 164, 36)
};

        public readonly static Color[] RockColors = new Color[]
        {
            new Color(147, 147, 147),
            new Color(88, 77, 98),
            new Color(111, 101, 125)
        };

        public readonly static Color[] StickColors = new Color[]
        {
            new Color(125, 86, 67),
            new Color(159, 120, 84),
            new Color(108, 60, 74)
        };

        public readonly static Color[] FlowerColors = new Color[]
        {
            new Color(255, 208, 72),
            new Color(182, 116, 94),
            new Color(29, 100, 64)
        };

        public readonly static Color[] BushColors = new Color[]
        {
            new Color(255, 43, 60),
            new Color(29, 100, 64),
            new Color(120, 44, 77)
        };




    }
}
