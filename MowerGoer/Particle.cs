﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MowerGoer
{
    public class Particle
    {
        private float _timeToLiveMs;
        private float _timeToLiveCounterMs;
        public Texture2D Texture { get; private set; }
        public Vector2 Position { get; private set; }
        public Vector2 Velocity { get; private set; }
        public float Angle { get; private set; }
        public float AngularVelocity { get; private set; }
        public Color Color { get; private set; }
        public float Scale { get; private set; }
        public bool IsAlive { get; private set; }

        public void Initialize(Texture2D texture, Vector2 position, Vector2 velocity, float angle, float angularVelocity, Color color, float scale, float timeToLiveMs)
        {
            Texture = texture;
            Position = position;
            Velocity = velocity;
            Angle = angle;
            AngularVelocity = angularVelocity;
            Color = color;
            Scale = scale;
            _timeToLiveMs = timeToLiveMs;
            _timeToLiveCounterMs = 0;
            IsAlive = true;
        }

        public void Update(GameTime gameTime)
        {
            // todo: multiply this by gametime
            Position += Velocity;
            Angle += AngularVelocity;

            _timeToLiveCounterMs += gameTime.ElapsedGameTime.Milliseconds;
            if (_timeToLiveCounterMs >= _timeToLiveMs)
            {
                IsAlive = false;
            }
        }
    }
}
