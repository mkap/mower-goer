﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace MowerGoer.States
{
    public class StartScreenState : State
    {
        private float _inputTimeElapsedMs;
        private bool _acceptInput;
        private float _fadeoutTimeElapsedMs;
        private bool _isFadingOut;
        private KeyboardState _previousKeyboardState;
        public StartScreenState(MainGame mainGame)
            : base(mainGame)
        {
        }

        public override void Update(GameTime gameTime)
        {
            if (_isFadingOut)
            {
                _fadeoutTimeElapsedMs += gameTime.ElapsedGameTime.Milliseconds;
                MediaPlayer.Volume = MathHelper.Lerp(0.1f, 0, _fadeoutTimeElapsedMs / Constants.StartScreenFadeoutTimeMs);
                if (_fadeoutTimeElapsedMs >= Constants.StartScreenFadeoutTimeMs)
                {
                    MediaPlayer.Volume = 0.1f;
                    MediaPlayer.Play(ResourceManager.GetSong(SongName.HappyLevel));
                    _mainGame.ChangeState(StateName.CountDown);
                }
            }
            else
            {
                _inputTimeElapsedMs += gameTime.ElapsedGameTime.Milliseconds;
                if (_inputTimeElapsedMs >= Constants.StartScreenInputTimeMs)
                {
                    _acceptInput = true;
                }

                if (_acceptInput)
                {
                    HandleInput();
                }
            }
        }
        public override void Draw(SpriteBatch spriteBatch)
        {
            // draw title screen
            spriteBatch.Begin(SpriteSortMode.Deferred, null, SamplerState.PointClamp, null, null, null, Matrix.CreateScale(4.0f));
            spriteBatch.Draw(ResourceManager.GetTexture(TextureName.TitleScreen), Vector2.Zero, Color.White);
            spriteBatch.End();

            // draw UI Elements 
            spriteBatch.Begin();
            DrawStringCentered(spriteBatch, "Cut as much grass as you can in 10 seconds!", FontName.DigitalDiscoFont, 400);
            DrawStringCentered(spriteBatch, "Avoid sticks, stones, bushes, and, most of all, flowers!", FontName.DigitalDiscoFont, 450);
            if (_acceptInput)
            {
                DrawStringCentered(spriteBatch, "Press ENTER to Start", FontName.DigitalDiscoFont, 540);
            }

            spriteBatch.End();
        }
        public override void OnEntry()
        {
            _fadeoutTimeElapsedMs = 0;
            _inputTimeElapsedMs = 0;
            _isFadingOut = false;
            _acceptInput = false;
            _currentLevel = Constants.StartingLevelIndex;
            _totalScore = 0;

            _player.Initialize();
            foreach (var level in _levels)
            {
                level.Initialize();
            }
        }

        public void HandleInput()
        {
            var keyboardState = Keyboard.GetState();
            if (keyboardState.IsKeyDown(Keys.Enter) && _previousKeyboardState.IsKeyUp(Keys.Enter))
            {
                ResourceManager.GetSoundEffect(SoundEffectName.Start).Play(0.1f, 0, 0);
                _isFadingOut = true;
            }

            _previousKeyboardState = keyboardState;
        }
    }
}
