﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MowerGoer.States
{
    public class GamePlayState : State
    {
        private KeyboardState _currentKeyboardState;
        private KeyboardState _previousKeyboardState;

        private int _gamePlayTimeSeconds;
        private float _gamePlayTimeElapsedMs;


        public GamePlayState(MainGame mainGame)
            : base(mainGame)
        {
        }
        public override void Update(GameTime gameTime)
        {
            _gamePlayTimeElapsedMs += gameTime.ElapsedGameTime.Milliseconds;

            if (_gamePlayTimeElapsedMs >= 1000f)
            {
                _gamePlayTimeElapsedMs = 0;
                _gamePlayTimeSeconds--;

                if (_gamePlayTimeSeconds == 0)
                {
                    _player.IsTurbo = false;
                    _mainGame.ChangeState(StateName.Stop);
                    ResourceManager.GetSoundEffect(SoundEffectName.Stop).Play(0.1f, 0, 0);
                    return;
                }
            }

            // handle player input
            HandleInput(gameTime);

            // update player entity
            _player.Update(gameTime);

            // cut the grass
            CutTheGrass();
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            // draw game elements
            spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.NonPremultiplied, SamplerState.PointClamp, null, null, null, Matrix.CreateScale(4.0f));
            DrawLevel(spriteBatch, Color.White);
            DrawObstacles(spriteBatch, Color.White);
            DrawPlayer(spriteBatch, Color.White);
            spriteBatch.End();

            // draw particles
            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.NonPremultiplied, SamplerState.PointClamp, null, null, null, Matrix.CreateScale(4.0f));
            DrawParticles(spriteBatch);
            spriteBatch.End();

            // draw UI Elements 
            spriteBatch.Begin();
            DrawString(spriteBatch, "1UP", FontName.DigitalDiscoFont, new Vector2(64, 0));
            DrawString(spriteBatch, _totalScore.ToString(), FontName.DigitalDiscoFont, new Vector2(32, 32));
            DrawStringCentered(spriteBatch, "HIGH SCORE", FontName.DigitalDiscoFont, 0);
            DrawStringCentered(spriteBatch, _highScore.ToString(), FontName.DigitalDiscoFont, 32);
            DrawString(spriteBatch, "TIME LEFT", FontName.DigitalDiscoFont, new Vector2(1080, 0));
            DrawString(spriteBatch, _gamePlayTimeSeconds.ToString(), FontName.DigitalDiscoFont, new Vector2(1150, 32));
            spriteBatch.End();
        }
        public override void OnEntry()
        {
            _gamePlayTimeElapsedMs = 0;
            _gamePlayTimeSeconds = Constants.RoundTimeSeconds;
        }

        private void HandleInput(GameTime gameTime)
        {
            _currentKeyboardState = Keyboard.GetState();

            if (_currentKeyboardState.IsKeyDown(Keys.A) || _currentKeyboardState.IsKeyDown(Keys.Left))
            {
                _player.UpdateSteeringAngle(-gameTime.ElapsedGameTime.Milliseconds * 0.001f);
            }

            if (_currentKeyboardState.IsKeyDown(Keys.D) || _currentKeyboardState.IsKeyDown(Keys.Right))
            {
                _player.UpdateSteeringAngle(gameTime.ElapsedGameTime.Milliseconds * 0.001f);
            }

            if (_currentKeyboardState.IsKeyDown(Keys.Space))
            {
                _player.IsTurbo = true;
            }
            else if (_previousKeyboardState.IsKeyDown(Keys.Space) && _currentKeyboardState.IsKeyUp(Keys.Space))
            {
                _player.IsTurbo = false;
            }

            // the gas is always down!
            _player.UpdateSpeed(gameTime.ElapsedGameTime.Milliseconds);


            _previousKeyboardState = _currentKeyboardState;
        }

        private void CutTheGrass()
        {
            var pixelsInRange = GetPixelsInRange();
            var texture = _levels[_currentLevel].LongGrassTexture;
            var textureArray = new Color[texture.Width * texture.Height];

            texture.GetData(textureArray);

            foreach (var pixel in pixelsInRange)
            {
                var index = pixel.X + pixel.Y * texture.Width;

                if (index > 0 && index < textureArray.Length)
                {
                    if (textureArray[index].A == 255)
                    {
                        textureArray[index].A = 0;
                        _totalScore++;
                        _player.CreateParticles(1, ObstacleType.None);
                        IncrementHitCount(ObstacleType.None);
                    }
                }

                foreach (var obstacle in _levels[_currentLevel].Obstacles)
                {
                    if (!obstacle.IsDestroyed && obstacle.Pixels.Contains(pixel))
                    {
                        _totalScore += obstacle.NegativePointValue;
                        obstacle.IsDestroyed = true;
                        ResourceManager.GetSoundEffect(SoundEffectName.Obstacle).Play(0.1f, 0, 0);
                        _player.HitObstacle(obstacle.ObstacleType);
                        IncrementHitCount(obstacle.ObstacleType);
                    }
                }
            }

            texture.SetData(textureArray);
        }

        private List<Point> GetPixelsInRange()
        {
            var centerPixel = new Point((int)Math.Round(_player.Position.X, 0), (int)Math.Round(_player.Position.Y, 0));
            var topLeft = new Point(centerPixel.X - 15, centerPixel.Y - 15);
            var bottomRight = new Point(centerPixel.X + 15, centerPixel.Y + 15);

            var pixels = new List<Point>();

            for (int i = topLeft.X; i < bottomRight.X; i++)
            {
                for (int j = topLeft.Y; j < bottomRight.Y; j++)
                {
                    var pixel = new Point(i, j);
                    if (_player.CuttingArea.ContainsPoint(pixel))
                    {
                        pixels.Add(pixel);
                    }
                }
            }

            return pixels;
        }

        private void IncrementHitCount(ObstacleType obstacleType)
        {
            switch (obstacleType)
            {
                case ObstacleType.None:
                    _grassCut++;
                    break;

                case ObstacleType.Sticks:
                    _sticksHit++;
                    break;

                case ObstacleType.Rocks:
                    _rocksHit++;
                    break;

                case ObstacleType.Flowers:
                    _flowersHit++;
                    break;

                case ObstacleType.Bush:
                    _bushesHit++;
                    break;
            }
        }
    }
}