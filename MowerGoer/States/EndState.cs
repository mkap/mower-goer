﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace MowerGoer.States
{
    class EndState : State
    {
        private float _inputTimeElapsedMs;
        private bool _acceptInput;
        private float _fadeoutTimeElapsedMs;
        private bool _isFadingOut;
        private KeyboardState _previousKeyboardState;

        public EndState(MainGame mainGame)
            : base(mainGame)
        {
        }

        public override void OnEntry()
        {
            _inputTimeElapsedMs = 0;
            _fadeoutTimeElapsedMs = 0;
            _acceptInput = false;
            _isFadingOut = false;
            
            // get and save high scores
            if (_totalScore > _highScore)
            {
                SaveHighScore();
            }
        }

        public override void Update(GameTime gameTime)
        {
            if (_isFadingOut)
            {
                _fadeoutTimeElapsedMs += gameTime.ElapsedGameTime.Milliseconds;
                MediaPlayer.Volume = MathHelper.Lerp(0.1f, 0, _fadeoutTimeElapsedMs / Constants.EndScreenFadeoutTimeMs);
                if (_fadeoutTimeElapsedMs >= Constants.EndScreenFadeoutTimeMs)
                {
                    MediaPlayer.Volume = 0.1f;
                    MediaPlayer.Play(ResourceManager.GetSong(SongName.SubduedTheme));
                    _mainGame.ChangeState(StateName.StartScreen);
                }
            }
            else
            {
                _inputTimeElapsedMs += gameTime.ElapsedGameTime.Milliseconds;
                if (_inputTimeElapsedMs >= Constants.EndScreenInputTimeMs)
                {
                    _acceptInput = true;
                }

                if (_acceptInput)
                {
                    HandleInput();
                }
            }

            _player.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.NonPremultiplied, SamplerState.PointClamp, null, null, null, Matrix.CreateScale(4.0f));
            DrawLevel(spriteBatch, Color.DimGray);
            DrawObstacles(spriteBatch, Color.DimGray);
            DrawPlayer(spriteBatch, Color.DimGray);
            spriteBatch.End();

            // draw UI Elements 
            spriteBatch.Begin();
            DrawStringCentered(spriteBatch, "WORK COMPLETE!", FontName.DigitalDiscoBigFont, 120);
            DrawStringCentered(spriteBatch, $"Final Score: {_totalScore}", FontName.DigitalDiscoFont, 260);
            if (_totalScore > _highScore)
            {
                DrawStringCentered(spriteBatch, "Congratulations, this is the new HIGH SCORE!", FontName.DigitalDiscoFont, 310);
            }
            else
            {
                DrawStringCentered(spriteBatch, $"High Score: {_highScore}", FontName.DigitalDiscoFont, 310);
            }
            DrawStringCentered(spriteBatch, "Thank you for playing!", FontName.DigitalDiscoFont, 420);
            if (_acceptInput)
            {
                DrawStringCentered(spriteBatch, "Press ENTER to Restart", FontName.DigitalDiscoFont, 540);
            }
            spriteBatch.End();
        }

        public void HandleInput()
        {
            var keyboardState = Keyboard.GetState();
            if (keyboardState.IsKeyDown(Keys.Enter) && _previousKeyboardState.IsKeyUp(Keys.Enter))
            {
                _isFadingOut = true;
            }

            _previousKeyboardState = keyboardState;
        }

        private void SaveHighScore()
        {
            try
            {
                using (var file = IsolatedStorageFile.GetMachineStoreForAssembly())
                {
                    using (var writer = new BinaryWriter(file.CreateFile(Constants.HighScoreFilename)))
                    {
                        writer.Write(_totalScore);
                    }
                }
            }
            catch(Exception e)
            {
                // do nothing
            }
        }
    }
}
