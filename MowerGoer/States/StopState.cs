﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MowerGoer.States
{
    public class StopState : State
    {
        private float _stopTimeElapsedMs;
        public StopState(MainGame mainGame)
            : base(mainGame)
        {
        }
        public override void Update(GameTime gameTime)
        {
            _stopTimeElapsedMs += gameTime.ElapsedGameTime.Milliseconds;
            if (_stopTimeElapsedMs >= Constants.StopTimeMs)
            {
                _mainGame.ChangeState(StateName.Results);
            }

            // update player entity
            _player.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.NonPremultiplied, SamplerState.PointClamp, null, null, null, Matrix.CreateScale(4.0f));

            DrawLevel(spriteBatch, Color.White);
            DrawObstacles(spriteBatch, Color.White);
            DrawPlayer(spriteBatch, Color.White);

            spriteBatch.End();

            // draw UI Elements 
            spriteBatch.Begin();
            DrawStringCentered(spriteBatch, "STOP!", FontName.DigitalDiscoBigFont, 240);
            spriteBatch.End();
        }

        public override void OnEntry()
        {
            _stopTimeElapsedMs = 0;
        }
    }
}
