﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MowerGoer.States
{
    public enum StateName
    {
        Loading, StartScreen, CountDown, GamePlay, Results, MapChange, End, Stop
    }

    public abstract class State
    {
        public State(MainGame mainGame)
        {
            _mainGame = mainGame;
        }

        protected static Player _player;
        protected static Level[] _levels = new Level[Constants.LevelCount];
        protected static int _currentLevel;
        protected static int _totalScore;
        protected static int _grassCut;
        protected static int _sticksHit;
        protected static int _rocksHit;
        protected static int _flowersHit;
        protected static int _bushesHit;
        protected static int _highScore;

        protected MainGame _mainGame;

        public abstract void Update(GameTime gameTime);
        public abstract void Draw(SpriteBatch spriteBatch);
        public abstract void OnEntry();

        protected void DrawLevel(SpriteBatch spriteBatch, Color color)
        {
            foreach (var tile in _levels[_currentLevel].Tiles)
            {
                spriteBatch.Draw(ResourceManager.GetTexture(TextureName.GroundTileset), tile.Position, tile.SourceRectangle, color, 0f, Vector2.Zero, 1.0f, SpriteEffects.None, 0.0f);
            }

            spriteBatch.Draw(_levels[_currentLevel].LongGrassTexture, Vector2.Zero, null, color, 0f, Vector2.Zero, 1.0f, SpriteEffects.None, 0.1f);
        }

        protected void DrawPlayer(SpriteBatch spriteBatch, Color color)
        {
            spriteBatch.Draw(_player.Texture, _player.Position + _player.DrawOffset, null, color, _player.Heading + MathHelper.PiOver2 * 3, new Vector2(16, 16), 1.0f, SpriteEffects.None, 0.2f);
        }

        protected void DrawObstacles(SpriteBatch spriteBatch, Color color)
        {
            foreach (var obstacle in _levels[_currentLevel].Obstacles)
            {
                if (obstacle.ObstacleType != ObstacleType.None)
                {
                    var spriteIndex = (int)obstacle.ObstacleType - 1;
                    var position = new Vector2(obstacle.Pixels[0].X - 6, obstacle.Pixels[0].Y - 6);
                    var texture = obstacle.IsDestroyed ? ResourceManager.GetTexture(TextureName.RuinedObstacleTileset) : ResourceManager.GetTexture(TextureName.ObstacleTileset);
                    spriteBatch.Draw(texture, position, new Rectangle(spriteIndex * 16, 0, 16, 16), color, 0f, Vector2.Zero, 1.0f, SpriteEffects.None, 0.15f);
                }
            }
        }

        protected void DrawParticles(SpriteBatch spriteBatch)
        {
            foreach (var particle in _player.GetParticles())
            {
                if (particle.IsAlive)
                {
                    spriteBatch.Draw(
                        texture: particle.Texture,
                        position: particle.Position,
                        sourceRectangle: null,
                        color: particle.Color,
                        rotation: particle.Angle,
                        origin: Vector2.Zero,
                        scale: particle.Scale,
                        effects: SpriteEffects.None,
                        layerDepth: 0.0f
                        );
                }
            }
        }

        protected void DrawString(SpriteBatch spriteBatch, string text, FontName fontName, Vector2 position)
        {
            var font = ResourceManager.GetFont(fontName);
            var shadowOffset = fontName == FontName.DigitalDiscoFont ? 4 : 8;
            spriteBatch.DrawString(font, text, new Vector2(position.X, position.Y + shadowOffset), Color.Black, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.0f);
            spriteBatch.DrawString(font, text, position, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.0f);
        }

        protected void DrawStringCentered(SpriteBatch spriteBatch, string text, FontName fontName, float verticalPosition)
        {
            var font = ResourceManager.GetFont(fontName);
            var shadowOffset = fontName == FontName.DigitalDiscoFont ? 4 : 8;
            var centerPosition = font.MeasureString(text).X / 2;
            spriteBatch.DrawString(font, text, new Vector2(640 - centerPosition, verticalPosition + shadowOffset), Color.Black, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.0f);
            spriteBatch.DrawString(font, text, new Vector2(640 - centerPosition, verticalPosition), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.0f);
        }
    }
}