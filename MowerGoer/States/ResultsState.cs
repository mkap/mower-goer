﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MowerGoer.States
{
    public class ResultsState : State
    {
        private float _inputTimeElapsedMs;
        private bool _acceptInput;
        private int _score;
        private int _sticksHitValue;
        private int _rocksHitValue;
        private int _flowersHitValue;
        private int _bushesHitValue;

        public ResultsState(MainGame mainGame)
            : base(mainGame)
        {
        }

        public override void Update(GameTime gameTime)
        {
            _inputTimeElapsedMs += gameTime.ElapsedGameTime.Milliseconds;
            if (_inputTimeElapsedMs >= Constants.ResultsInputTimeMs)
            {
                _acceptInput = true;
            }

            // update player entity
            _player.Update(gameTime);

            if (_acceptInput)
            {
                HandleInput();
            }
        }
        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.NonPremultiplied, SamplerState.PointClamp, null, null, null, Matrix.CreateScale(4.0f));
            DrawLevel(spriteBatch, Color.White);
            DrawObstacles(spriteBatch, Color.White);
            DrawPlayer(spriteBatch, Color.White);
            spriteBatch.End();

            // draw UI Elements 
            spriteBatch.Begin();
            DrawStringCentered(spriteBatch, "RESULTS", FontName.DigitalDiscoBigFont, 64);
            DrawString(spriteBatch, $"Grass x {_grassCut} = {_grassCut}", FontName.DigitalDiscoFont, new Vector2(420, 200));
            DrawString(spriteBatch, $"Sticks x {_sticksHit} = {_sticksHitValue}", FontName.DigitalDiscoFont, new Vector2(420, 250));
            DrawString(spriteBatch, $"Stones x {_rocksHit} = {_rocksHitValue}", FontName.DigitalDiscoFont, new Vector2(420, 300));
            DrawString(spriteBatch, $"Bushes x {_bushesHit} = {_bushesHitValue}", FontName.DigitalDiscoFont, new Vector2(420, 350));
            DrawString(spriteBatch, $"Flowers x {_flowersHit} = {_flowersHitValue}", FontName.DigitalDiscoFont, new Vector2(420, 400));
            DrawStringCentered(spriteBatch, $"Score: {_score}", FontName.DigitalDiscoFont, 500);
            DrawStringCentered(spriteBatch, $"Total Score: {_totalScore}", FontName.DigitalDiscoFont, 550);

            if (_acceptInput)
            {
                DrawStringCentered(spriteBatch, "Press ENTER to Continue", FontName.DigitalDiscoFont, 640);
            }
            
            spriteBatch.End();
        }
        public override void OnEntry()
        {
            _inputTimeElapsedMs = 0;
            _acceptInput = false;
            CalculateScoreValues();

        }

        public void HandleInput()
        {
            var keyboardState = Keyboard.GetState();
            if (keyboardState.IsKeyDown(Keys.Enter))
            {
                _mainGame.ChangeState(StateName.MapChange);
            }
        }

        private void CalculateScoreValues()
        {
            _sticksHitValue = _sticksHit * Constants.ObstacleValues[3];
            _rocksHitValue = _rocksHit * Constants.ObstacleValues[1];
            _flowersHitValue = _flowersHit * Constants.ObstacleValues[2];
            _bushesHitValue = _bushesHit * Constants.ObstacleValues[4];
            _score = _grassCut + _sticksHitValue + _rocksHitValue + _flowersHitValue + _bushesHitValue;
        }
    }
}
