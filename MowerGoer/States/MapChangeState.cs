﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MowerGoer.States
{
    public class MapChangeState : State
    {
        private float _mapChangeTimeElapsedMs;
        private int _wipeTexturePosition;
        private bool _mapChanged;

        public MapChangeState(MainGame mainGame)
            : base(mainGame)
        {
        }

        public override void Update(GameTime gameTime)
        {
            _mapChangeTimeElapsedMs += gameTime.ElapsedGameTime.Milliseconds;
            if (!_mapChanged && _mapChangeTimeElapsedMs >= Constants.MapChangeTimeMs / 2)
            {
                if (_currentLevel == Constants.LevelCount - 1)
                {
                    _mainGame.ChangeState(StateName.End);
                    return;
                }
                else
                {
                    _currentLevel++;
                    _levels[_currentLevel].Initialize();
                    _player.Initialize();

                    _mapChanged = true;
                }
            }

            _wipeTexturePosition = (int)Math.Round(MathHelper.SmoothStep(-1280, 1280, _mapChangeTimeElapsedMs / Constants.MapChangeTimeMs), 0);

            if (_wipeTexturePosition >= 1280)
            {
                _mainGame.ChangeState(StateName.CountDown);
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.NonPremultiplied, SamplerState.PointClamp, null, null, null, Matrix.CreateScale(4.0f));

            DrawLevel(spriteBatch, Color.White);
            DrawObstacles(spriteBatch, Color.White);
            DrawPlayer(spriteBatch, Color.White);

            spriteBatch.End();

            spriteBatch.Begin();
            spriteBatch.Draw(ResourceManager.GetTexture(TextureName.PixelParticle), new Rectangle(_wipeTexturePosition, 0, 1280, 720), Color.Black);
            spriteBatch.End();
        }

        public override void OnEntry()
        {
            _mapChangeTimeElapsedMs = 0;
            _wipeTexturePosition = -320;
            _mapChanged = false;
            ResetRoundCounters();
        }

        private void ResetRoundCounters()
        {
            _grassCut = 0;
            _sticksHit = 0;
            _rocksHit = 0;
            _flowersHit = 0;
            _bushesHit = 0;
        }
    }
}
