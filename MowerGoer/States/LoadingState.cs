﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ContentPipelineExtension;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;

namespace MowerGoer.States
{
    public class LoadingState : State
    {
        private int _tilesetColumns;
        public LoadingState(MainGame mainGame)
            : base(mainGame)
        {
        }

        public override void Update(GameTime gameTime)
        {

        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            
        }

        public override void OnEntry()
        {
            MediaPlayer.Play(ResourceManager.GetSong(SongName.SubduedTheme));
            BuildLevels();
            LoadHighScore();
            if (_highScore == 0)
            {
                _highScore = Constants.DefaultHighScore; 
            }
            _player = new Player();
            _mainGame.ChangeState(StateName.StartScreen);
        }

        private void BuildLevels()
        {
            _tilesetColumns = ResourceManager.GetTexture(TextureName.GroundTileset).Width / Constants.TileSize;
            for (int i = 0; i < _levels.Length; i++)
            {
                _levels[i] = BuildLevel(ResourceManager.LoadMap($"Level_{i}"), ResourceManager.LoadTexture("LongGrass"));
                _levels[i].Name = Constants.LevelNames[i];
                _levels[i].Number = i;
            }
        }

        // todo: replace magic numbers
        private Level BuildLevel(TiledMap map, Texture2D longGrassTexture)
        {
            var level = new Level(Constants.MapRows, Constants.MapColumns, longGrassTexture);
            for (int i = 0; i < Constants.MapRows; i++)
            {
                for (int j = 0; j < Constants.MapColumns; j++)
                {
                    var tileIndex = map.Layers[0].Data[i * Constants.MapColumns + j] - 1;
                    var sourceX = tileIndex % _tilesetColumns * 16;
                    var sourceY = tileIndex / _tilesetColumns * 16;
                    var tileType = tileIndex == 0 ? TileType.Grass : TileType.Other;
                    var objectIndex = map.Layers[1].Data[i * Constants.MapColumns + j];
                    var objectType = objectIndex == 0 ? ObstacleType.None : (ObstacleType)objectIndex - 2;

                    if (objectType != ObstacleType.None)
                    {
                        var pixels = new List<Point>();
                        for (int y = 6; y < 10; y++)
                        {
                            for (int x = 6; x < 10; x++)
                            {
                                pixels.Add(new Point(j * 16 + x, i * 16 + y));
                            }
                        }

                        var value = Constants.ObstacleValues[(int)objectType];

                        var obstacle = new Obstacle(pixels.ToArray(), value, objectType);
                        level.Obstacles.Add(obstacle);
                    }

                    var tile = new Tile(
                        new Vector2(16 * j, 16 * i),
                        ResourceManager.GetTexture(TextureName.GroundTileset),
                        new Rectangle(sourceX, sourceY, 16, 16),
                        tileType
                        );

                    level.Tiles[i, j] = tile;

                    if (tile.TileType.Equals(TileType.Grass))
                    {
                        level.GrassTiles++;
                    }
                }
            }
            return level;
        }

        private void LoadHighScore()
        {
            try
            {
                using (var file = IsolatedStorageFile.GetMachineStoreForAssembly())
                {
                    if (file.FileExists(Constants.HighScoreFilename))
                    {
                        using (var reader = new BinaryReader(file.OpenFile(Constants.HighScoreFilename, FileMode.OpenOrCreate)))
                        {
                            _highScore = reader.ReadInt32();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                // do nothing
            }
        }
    }
}
