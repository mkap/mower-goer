﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MowerGoer.States
{
    public class CountdownState : State
    {
        private float _countdownTimeElapsedMs;
        private string _countdownText;
        private float _countdownTextVerticalPosition;
        private int _countdownStep;
        public CountdownState(MainGame mainGame)
            : base(mainGame)
        { 
        }
        public override void Update(GameTime gameTime)
        {
            _countdownTimeElapsedMs += gameTime.ElapsedGameTime.Milliseconds;

            if (_countdownTimeElapsedMs >= 5000f && _countdownStep == 4)
            {
                _mainGame.ChangeState(StateName.GamePlay);
            }
            else if (_countdownTimeElapsedMs >= 4000f && _countdownStep == 3)
            {
                _countdownText = "GO!";
                _countdownTextVerticalPosition = 240f;
                _countdownStep = 4;
                ResourceManager.GetSoundEffect(SoundEffectName.Go).Play(0.1f, 0, 0);
            }
            else if (_countdownTimeElapsedMs >= 3000f && _countdownStep == 2)
            {
                _countdownText = "1";
                _countdownTextVerticalPosition = 240f;
                _countdownStep = 3;
                ResourceManager.GetSoundEffect(SoundEffectName.Countdown).Play(0.05f, 0, 0);
            }
            else if (_countdownTimeElapsedMs >= 2000f && _countdownStep == 1)
            {
                _countdownText = "2";
                _countdownTextVerticalPosition = 240f;
                _countdownStep = 2;
                ResourceManager.GetSoundEffect(SoundEffectName.Countdown).Play(0.05f, 0, 0);
            }
            else if (_countdownTimeElapsedMs >= 1000f && _countdownStep == 0)
            {
                _countdownText = "3";
                _countdownTextVerticalPosition = 240f;
                _countdownStep = 1;
                ResourceManager.GetSoundEffect(SoundEffectName.Countdown).Play(0.05f, 0, 0);
            }

            _player.Update(gameTime);
        }
        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.NonPremultiplied, SamplerState.PointClamp, null, null, null, Matrix.CreateScale(4.0f));

            DrawLevel(spriteBatch, Color.White);
            DrawObstacles(spriteBatch, Color.White);
            DrawPlayer(spriteBatch, Color.White);

            spriteBatch.End();

            // draw UI Elements 
            spriteBatch.Begin();
            DrawStringCentered(spriteBatch, $"Level {_levels[_currentLevel].Number}", FontName.DigitalDiscoBigFont, 32);
            DrawStringCentered(spriteBatch, _levels[_currentLevel].Name, FontName.DigitalDiscoFont, 132);
            DrawStringCentered(spriteBatch, _countdownText, FontName.DigitalDiscoBigFont, _countdownTextVerticalPosition);
            spriteBatch.End();
        }

        public override void OnEntry()
        {
            _countdownText = string.Empty;
            _countdownTimeElapsedMs = 0;
            _countdownStep = 0;
        }
    }
}
