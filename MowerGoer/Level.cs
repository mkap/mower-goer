﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MowerGoer
{
    public class Level
    {
        public Tile[,] Tiles { get; private set; }
        public Texture2D LongGrassTexture { get; set; }
        public int GrassTiles { get; set; }
        public string Name { get; set; }
        public int Number { get; set; }
        public List<Obstacle> Obstacles { get; set; } = new List<Obstacle>();

        public Level(int rows, int columns, Texture2D longGrassTexture)
        {
            Tiles = new Tile[rows, columns];
            LongGrassTexture = longGrassTexture;
        }

        public void Initialize()
        {
            var textureArray = new Color[LongGrassTexture.Width * LongGrassTexture.Height];
            LongGrassTexture.GetData(textureArray);

            for (int i = 0; i < textureArray.Length; i++)
            {
                textureArray[i].A = 255;
            }

            LongGrassTexture.SetData(textureArray);

            foreach (var obstacle in Obstacles)
            {
                obstacle.IsDestroyed = false;
            }
        }
    }
}
